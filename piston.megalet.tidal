-- megalet for piston.tidal
let d'drum = d1 -- k, sn
    d'perc = d2 -- wt, wtsn, hh
    d'gnome = d3 -- gnome, cymb
    d'bass = d4 -- dubb, k
    d'fly = d5 -- fly
    d'bz = d6 -- bz, bzl, ff
    d'shard = d7 -- ice, plode
    d'ant = d9 -- ant
    d'misc = d8 -- overflow channel for a few cases
    d'cymb = d10 -- cymb
    msil = mapM_ ($ silence)
    k = s "k"
    k1 = s "k:1"
    sn = s "[~ sn]/2"
    bass'params = (# gain "0.7").
                  (# hcutoff "0.022").
                  (# hresonance "0.001")
    bass' c = rev . chop c $ bass'params $ s "[dubb dubb:1]/2"
    fly'1 = slow 16 $ jux (0.5 <~) $ rev . striate 256 $ s "fly"
            # hcutoff "0.09"
            # hresonance "0.06"
    fly'1f = whenmod 5 2 (# vowel "i o") $ whenmod 3 1 (slow 2) $ brak $ fly'1
    snare43'1 = density (4%3) $ s "[wtsn ~ wt]"
    shnare'1 = s "[~ ~ sn [~ sn*3]/2]/2"
    kick38'1 = s "[k(3,8)]/2"
    gnome'1 = within (0, 0.5) rev $ chop 8 $ s "[gnome(3,8)]/2"
              # gain "0.8"
              # speed ((+0.8).(/5) <$> slow 1.1 sinewave1)
    bass'1'm = every 2 $ within (0.25, 0.75) (0.5 ~>)
    wt'2 = s "wt*2"
           # vowel "[u e u a]/2"
           # end "0.15"
    bass'2 = slow 8 $ ((1%8) <~) $ chop 32 $
             bass'params $ s "[dubb dubb:1]/2"
             # speed "1.2"
             # shape ((/2) <$> density 4 sinewave1)
             # cut "1"
    ant'2 = slow 2 $ smash 16 [1,2] $ s "ant:1"
            # cutoff "0.08"
            # resonance "0.01"
    shard'2 = chop 16 $ s "[ice [~ ice] ~ ~]/4"
              # gain "0.7"
    shard'2f = every 2 (density 1.5) $ whenmod 5 3 (density 2) $ rev $ shard'2
               # shape ((+0.25).(/2) <$> density 8 saw1)
    fly'2 = chop 64 $ s "[fly ~ [fly ~ ~ fly] ~]/4"
            # cut "1"
            # begin "0.3"
            # end "0.35"
            # shape ((+0.25).(/2) <$> density 8 sinewave1)
            # speed ((+0.92) <$> density (5%4) saw1)
    cymb'2 g = within (0.5,1) (# vowel "e") $
               (0.5 ~>) $ chop 2 $ s "[cymb*4 cymb*3]/8"
               # shape ((/3) <$> density 3.6 saw1)
               # gain g
    bass'3 v c b = (4 <~) $ every 2 (0.5 <~) $
                   slow 4 .
                   (if b then brak else id) $
                   rev . chop 16 $
                   bass'params $ s "[dubb dubb:1]/2"
                   # cut c
                   # vowel v
    bz'3 = s "[ff(3,8) [~ [ff bz]]]/4"
           # end "0.3"
           # shape "0 0.25"
           # gain "0.7"
    drum'3 = overlay (s "[~ sn]/2") $ s "[k*2 [~ k*2] k ~ k*2 ~ k*2 ~]/8"
    bass2'3 = slow 8 $ chop 16 $ bass'params $ s "[dubb dubb:1]/2"
              # cut "1"
              # speed "0.2"
              # vowel "e"
    perc'3 = s "[[k:1 [~ k:1] k:1 [~ k:1]] sn]/8"
    perc'3'delay = (# delay "0.3").
                   (# delayfeedback "0.5").
                   (# delaytime "0.766") --(138/60)/3
    shard'3f = iter 4 . density 2 $ (0.5 <~) $ ((1%16) <~) $ shard'2
               # vowel "e o"
               # gain "0.6"
    drum5'4 = s "[[~ [wt wtsn]] ~ wt ~]/5"
    plox'4 so = within (0.5, 0.75) (iter 4) $
                whenmod 3 2 (slow 2) $
                slow 8 $ every 2 rev $ striate 128 $ s so
                # cutoff "0.02"
                # resonance "0.001"
                # speed "0.7"
                # gain "0.7"
    bass'4 = whenmod 5 3 (density (3%2) . brak) $
             bass'params $
             rev $ (0.5 ~>) $ slow 2 . chop 8 $ s "[dubb dubb:1]/2"
             # speed "0.5"
             # cut "1"
             # end "0.5"
    bz'4 = (0.5 <~) $ slow 16 . chop 32 $ s "bzl"
           # n (density 1.5 "0 1")
           # speed ((+1) <$> density 1.7 sinewave1)
           # gain "0.9"
    bzox'4 so = whenmod 7 4 (within (0.5, 1) (density 2)) $
                (0.5 <~) $
                slow 16 . chop 32 $ s so
                # n (density 1.5 "0 1")
                # speed ((+1.6) <$> density 2.2 sinewave1)
                # pan ((+0.25).(/2) <$> density 4.1 sinewave1)
    perc'5 = s "~ wt wt wtsn/8"
             # shape ((/2) <$> density 1.2 saw1)
    drum'5 = s "{[~ ~ ~ k:1], [~ sn]/2}"
    drum'5f = overlay k $ drum'5
    bass'5 = chop 16 $ bass'params $ s "[~ dubb]"
             # n (slow 4 "0 1")
             # cut "1"
    bass'5f = every 2 (density 2) $ whenmod 3 2 (density 1.6) $ bass'5
              # speed ((+0.75).(/2) <$> slow 1.3 sinewave1)
    fly'5 b = whenmod 5 3 rev $
              (if b then brak else id) $
              slow 16 $ jux ((1.5 ~>).rev) $
              chop 64 $ bass'params $ s "fly:1"
              # speed "0.7"
              # shape ((/2) <$> density 2.1 saw1)
    bass2'5 = chop 12 $ s "[~ ~ [~ k:1] !]/4"
              # speed "0.9"
    bass2'5b = whenmod 4 3 (density 1.8) $ bass2'5
               # shape ((/3) <$> density 3 sinewave1)
               # delay "0.3"
               # delayfeedback "0.2"
               # delaytime "0.2875" --(138/60)/8
    gnome'5 sm f = smash 64 sm $
                   (# speed "1.2").
                   (# shape ((/2) <$> ((0.25 ~>) . density 3) sinewave1)).
                   (# gain "0.6") $
                   s "~ gnome"
                   # pan (f $ slow 4 sinewave1)
    sn'6 = s "[~ sn ~ sn*2]/8"
    bass'6 = (# gain ((*0.7) <$> saw1)) $ bass'5f
    bass2'6 = within (0, 0.5) (slow (7%6)) $
              every 2 (0.5 <~) $ slow 8 $
              bass' 32
    plode'6 = superimpose (slow (5%3) . iter 2) $
              (# speed ((+0.7) <$> slow 1.1 sinewave1)) $
              chop 32 $ s "[plode(5,8)]/8"
              # cut "1"
    cymb'6 = s "[cymb*8 ~]/8"
             # gain ((+0.6).(/5) <$> density 3.6 saw1)
             # cut "1"
    cymb'6f = density (5%4) $ whenmod 9 4 (density 1.5) $ cymb'6
    drum'6 = s "{k, [~ ~ ~ k:1] [~ k:1]}"
    wtsn'6 = (3 ~>) $ within (0.25, 0.75) rev $
             slowspread ($) [chop 16, chop 32] $ s "[wtsn(3,8)]/2"
             # speed ((+0.8).(/2) <$> density 1.8 sinewave1)
    gnome'6 d = iter 4 $ density (9%8) $
                within (0, 0.5) rev $
                smash 8 [1,2] $ s "[gnome(3,8)]/2"
                # gain "0.8"
                # speed ((+0.76).(/d) <$> slow 1.1 sinewave1)
    shard'7 dt = slow 5 . chop 15 $ s "ice"
                 # gain "0.7"
                 # resonance "0.4"
                 # cutoff "0.2"
                 # delay "0.7"
                 # delayfeedback "0.3"
                 # delaytime dt
    bass'7 f = slow 8 $ f $
               slowspread ($) [rev.chop 12, rev.chop 24] $
               bass'params $ s "[dubb dubb:1]/2"
    bass'7'm f = (f . (/2) <$> density (5%4) sinewave1)
    fly'7 = slow (4%3) .
            slowspread ($) [slow 1, density (5%4)] $
            slow 12 $
            slowspread striate [24,36] $ s "fly:1"
            # gain "0.7"
    bz'8 = s "[[ff bz]*2 [~ bz*2]]/2"
           # n (slow 2 $ run 5)
           # begin "0.7"
           # end "0.85"
           # hcutoff "0.04"
           # hresonance "0.1"
           # shape ((/5) <$> slow (5%3) sinewave1)
