# Anny: Piston LP

A live-coded exploration of rhythm & self-reference over 8 movements, written throughout 2015 and performed at algoraves in the UK. Originally performed live @ Texture, Manchester, 12th November 2015. Album released 20th November 2015.

_Anny is formerly Anny FM._

## Listen to the album!

- [Buy Piston LP on Bandcamp](http://anny.audio/album/piston-lp) (limited edition CD also available)
- [Stream Piston LP on SoundCloud](https://soundcloud.com/anny-fm/piston-lp)

## Source files

`piston.tidal` is the full composition for Tidal 0.6 (also compatible with 0.7) and will be maintained long term.

`piston.megalet.tidal` contains a single `let` directive that when eval'd with `C-c C-e` will initialize all variables used in the composition.

`piston-notes.tidal` are notes to myself for reference in a live performance.

## Sample attributions

TBC (I gotta go look them up!)

## License

Album recording is copyright &copy; Aneurin "Anny" Barker Snook 2015, but the source code and samples are all royalty free. Remix to your heart's content!